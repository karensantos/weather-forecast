import axios from 'axios';

const api = axios.create({
    baseURL: 'https://api.hgbrasil.com/weather?format=json-cors&key=8c67c398'
});

export default api;