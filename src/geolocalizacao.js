import api from './weather';

class App {
    constructor(){
        this.sectionDay = document.querySelector('#section-day');
        this.city = document.querySelector('#city');
        this.sectionForecast = document.querySelector('#section-forecast');
    }

    getLocation(){
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showPosition,showError);
        }
        else{
            alert("O navegador não tem suporte a Geolocalização!");
        }
    }

    setLoading() {
        /*$(window).load(function() {
            document.getElementById("loading").style.display = "none";
            document.getElementById("conteudo").style.display = "inline";
       })*/
        
    }

    showPosition(position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        async () => {
            this.getLocation();
            try{
                const response = await api.get('&woeid=455826');
                const {city_name, temp, date, description} = response.data.results;
                const forecast = response.data.results.forecast;
                console.log(forecast);
                console.log(response.data.results.city);
                console.log(city_name, temp +"°C "+  date, description);
            }catch(err){
                console.warn('Erro na requisição');
            }
        }
    }

    showError(error){
        switch(error.code){
            case error.PERMISSION_DENIED:
            x.innerHTML="Usuário rejeitou a solicitação de Geolocalização."
            break;
            case error.POSITION_UNAVAILABLE:
            x.innerHTML="Localização indisponível."
            break;
            case error.TIMEOUT:
            x.innerHTML="A requisição expirou."
            break;
            case error.UNKNOWN_ERROR:
            x.innerHTML="Algum erro desconhecido aconteceu."
            break;
        }
    }

    
}


//implementação de retorno de mensagens emc aso de possíveis erros
var msgAlert = document.getElementsByClassName('alert');
var caontiner = document.getElementsByClassName('.container');

function setLoading( loading = true){
    if(loading === true){
        alert("to aqui");
        msgAlert.removeAttribute("hidden");
        let loadingEl = document.createElement('p');
        loadingEl.appendChild(document.createTextNode('Carregando...'));
        loadingEl.setAttribute('id', 'loading');

        container.appendChild(loadingEl);
    }else{
        document.getElementById('loading').remove();

    }

    
}