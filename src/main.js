import api from './weather';

class App {
    constructor(){
        this.city = document.querySelector('#city');
        this.temperature = document.querySelector('#temp');
        this.update = document.querySelector('#update');
        this.iconDay = document.querySelector('#icon-day');
        this.descriptionDay = document.querySelector('#description');
        this.sectionForecast = document.querySelector('#section-forecast');
    }

    async startApp(){
        try{
            const response = await api.get('&woeid=455826');
            const forecast = response.data.results.forecast;
            this.loadingDay(response);
            this.renderForecast(forecast);

            //Console.log: teste
            console.log(response.data.results);
        }catch(err){
            console.warn('Erro na requisição');
        }
    } 

    //Render info section day:
    loadingDay(response){
        const {city_name, time, temp, date, description, img_id} = response.data.results;
        const day = `${city_name}, ${date}`;
        const tmp = `${temp} °C`;
        let linkIcon = `https://assets.hgbrasil.com/weather/images/${img_id}.png`;
        let outTime = time.replace(":", "h");
        
        this.update.appendChild(document.createTextNode(`Last update ${outTime}min`));
        this.iconDay.setAttribute('src', linkIcon);
        this.temperature.appendChild(document.createTextNode(tmp));
        this.city.appendChild(document.createTextNode(day));
        this.descriptionDay.appendChild(document.createTextNode(description));
    } 

    //Render info section forecast:
    renderForecast(forecast){
        forecast.forEach(element => {
            let div = document.createElement('div');
            let {date, weekday, max, min, description} = element;
            
            let textDate = document.createElement('p');
            let textDesc = document.createElement('p');
            let textTempMax = document.createElement('p');
            let textTempMin = document.createElement('p');
            let breakLine = document.createElement('br');
            
            textDate.appendChild(document.createTextNode(`${date} - ${weekday}`));
            textTempMax.appendChild(document.createTextNode(`Max: ${max} °C`)); 
            textTempMin.appendChild(document.createTextNode(`Min: ${min}`)); 
            textDesc.appendChild(document.createTextNode(`Previsão: ${description}`));
            
            div.appendChild(textDate);
            div.appendChild(textTempMax);
            div.appendChild(breakLine);
            div.appendChild(textTempMin);
            div.appendChild(textDesc);

            textDate.setAttribute('class', 'pt-2');
            div.setAttribute('class', 'card bg-light m-4 col-4 col-sm-3 col-md-3 col-lg-3 forecast');
            
            this.sectionForecast.appendChild(div);
           console.log(element);
        });
    }
}

new App().startApp();